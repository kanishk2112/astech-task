
      function initMap() {

		  var pointA = new google.maps.LatLng(18.9398208, 72.8354676),
		    pointB = new google.maps.LatLng(19.0895595, 72.8656144),
		    myOptions = {
		      zoom: 16,
		      center: pointA
		    }
        var map = new google.maps.Map(document.getElementById('map'), myOptions);

        var directionsDisplay = new google.maps.DirectionsRenderer({map:map});
        var directionsService = new google.maps.DirectionsService;

        directionsDisplay.setMap(map);

        calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB);
      }

      function calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB) {
        directionsService.route({
          origin: pointA,
          destination: pointB,
          travelMode: google.maps.TravelMode.DRIVING,
          drivingOptions: {
          	departureTime: new Date(Date.now()),
          	trafficModel: 'bestguess'
          }
        }, function(response, status) {
          if (status == 'OK') {
            directionsDisplay.setDirections(response);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }

      // Add a bookmark functionality
      $(document).ready(function() {
        
        $("#bookmarkme").on('click', function(event) {
          event.preventDefault();
          if (window.sidebar && window.sidebar.addPanel) { // Mozilla Firefox Bookmark
            window.sidebar.addPanel(document.title, window.location.href, '');
          } else if (window.external && ('AddFavorite' in window.external)) { // IE Favorite
            window.external.AddFavorite(location.href, document.title);
          } else if (window.opera && window.print) { // Opera Hotlist
            this.title = document.title;
            return true;
          } else { // webkit - safari/chrome
            alert('Press ' + (navigator.userAgent.toLowerCase().indexOf('mac') != -1 ? 'Command/Cmd' : 'CTRL') + ' + D to bookmark this page.');
          }
          
        });
      });