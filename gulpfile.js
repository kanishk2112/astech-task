var gulp = require('gulp');
var uglify = require('gulp-uglify');

gulp.task('minififyJS', function() {
	gulp.src('assets/js/*.js')
	.pipe(uglify())
	.pipe(gulp.dest('dist/js'));
});

gulp.task('default', ['minififyJS']);